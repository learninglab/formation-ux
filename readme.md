# Formation UX Mobile

Prise de notes en formation ux mobile du 16 au 18 décembre 2019.

## Plan

- [Discussion préliminaire](#discussion-préliminaire)
- [Cours ergonomie Web](#cours-ergonomie-web)
- [12 règles d'ergonomie web](#12-règles-dergonomie-web-par-amélie-boucher)
- [Définir la cible](#définir-la-cible)
- [Tester la cible](#tester-la-cible)

## Discussion préliminaire

- Suggestion ux elearning : Udemy, Socrative (Quiz)
- Présentation : Inria Learning Lab, Graphistes concessions automobiles

## Cours ergonomie Web

### Ergonomie

Histoire :

- Antiquité (prémisses)
- Travail à la chaine
- Années 50 : Alain Wisner (industrie)

Définition :

- Connaissance des tâches à acomplir
- Connaissance de ceux qui doivent accomplir les tâches
- Ergonomie > ergonomie IHM > ergonomie web

Pour être ergonomique :

- Utilité
- Bonne utilisabilité

Utilité :

- A quoi sert mon site ? fonction principale ?
- Utilité globale
    - Vendre des produits, protéger des informations
    - Plusieurs niveaux d'utilité (achat..)
- Micro utilités : services ou applications supportant l'utilité générale
    - fonctionnalités et infos complémentaires
    - ne sont pas mineures mais complètent l'utilité globale
    - créées en se mettant à la place de l'utilisateur
    - utile à au moins un des personas (prend de la place donc doivent être nécessaire)
    - Exemple appli emploi : 
      - Utilité globale : mettre en relation des candidats et des employeurs
      - Micro utilité : profil, recherche, filtre, formulaire, etc
- Norme ISO 9241 : efficacité, efficience, statisfaction pour atteindre des objectifs dans un contexte d'utilisation (exemple : tester dans des conditions réels)
    - Réussir à faire ce qu'on veut faire
    - Le faire facilement et rapidement
    - Etre satisfait

### UX design

**Définition :**

Satisfaction liée à l'utilisation d'une fonctionnalités

- sur la forme
- sur le fond
- sur la manière d'y accéder
- sur l'impact de notoriété du diffuseur du support

3 facteurs :

- Facteur humain
- Design
- Technologie

Diagramme design :

Besoins et objectifs (client et créateur) > specs > design d'intéraction > design d'informations > design visuel

Diagramme facteur humain :

Système perceptif > système cognitif > système moteur 
Output < processeur < input

- Loi de proximité :
    - Regroupé les contenus par similarité
    - Quelques pixels peuvent faire la différence
- Loi de similarité par la couleur
- Loi de similarité par la taille
- Loi de similarité par la forme

**=> Théories de la Gestalt** (psychologie de la forme)

Exemples : 
- compter les 'f' dans le texte (trompé par le cerveau)
- cercle derrière un carré (en fait ce n'est pas un cercle mais le reste est caché)
- damiers case de la même couleur à cause de l'ombre (importance du contexte)
- interférence couleur/action


Loi de Fitts : le temps pour atteindre un bouton est proportionnel à sa taille et à sa distance

Concept d'affordance : 
- Exemple : tirer une porte avec une poignée alors qu'il fallait pousser
- Attention aux affordances erronées

Loi de Hick : nombre reduit d'éléments = plus facile à décider
- Plusieurs choix simples, plutôt que un seul choix compliqué

Patterns (theory of fun) : 
Content <- Reconnu <- Element nouveau -> Pas reconnu -> Pas content

=> On aime extraire du sens à un élément nouveau

Nombre magique de Miller : maximum 7 objets à mémoriser (voir 3 ou 4)

Faciliter la mémorisation : 
- Le premeir et le dernier
- Organisation
- Répétition
- Impliquer l'interaction
- S'appuyer sur la mémoire à long terme

Point focal : loi de la Gestalt

### Outils

- Checklist Opquast (qualité web, accessibilité, green IT, etc)


## 12 règles d'ergonomie web par Amélie Boucher

1. Architecture : le site est bien rangé
2. Organisation visuelle : la page ets bien rangé
3. Cohérence : le site capitalise sur l'apprentissage interne
4. Conventions : le site capitalise sur l'apprentissage externe
5. Information : le site informe l'internaute et lui répond
6. Compréhension : les mots et symboles sont choisis minutieusement
7. Assistance : le site aide et dirige l'internaute
8. Gestion des erreurs : le site prévoit que l'internaute se trompe
9. Rapidité : l'internaute ne perd pas son temps
10. Liberté : c'est l'internaute qui commande
11. Accessibilité : une site facile d'accès pour tous
12. Satisfaction de l'internaute

## Définir la cible

Mieux connaitre ses utilisateurs : 

- Trouver toutes les sources indiquant qui sont ou devraient être nos utilisateurs en interne
- Exploiter les statistiques du site s'il y en a
- Interroger les internautes (ne pas trop forcer l'incitation)

Les statistiques de visite : 

- Qualité des visiteurs en termes fonctionnels : 
    - Pages les plus visitées
    - Nombre de pages vues
    - Eléments recueillant le plus de clics
    - Taux d'abandon
    - etc
    
- Logiciels complémentaires:
    - Relevé de clics (utile pour les affordances erronés)
    - Eye tracking

- Interroger
    - Les clients, sur le web ou boutique
    - Réaliser des sondages (même sur la concurrence)

**3 critères qui définissent une population cible :**
- Qui ? Client, utilisateur, cible
- Quoi ? Objectifs, besoins, fonctionnalités
- Comment ? Manière dont les gens utilissent l'interface

### Personas

- Intérêt : 
    - Forcer à se pencher réellement sur la cible
    - Humaniser la cible
        - Caractéristiques, histoires, envies, etc
        - Visage, nom, prénom, métier, etc
- Types de personas : 
    - Primaires : coeur de cible, utilisateur qui vient le plus souvent sur le site, aura le dernier mot en cas de désaccord
    - Secondaires : cibles secondaires
    - Tertiaires : cible plus large ou à côté (prescripteur)
    - Ante-persona : personnes hors cible qu'on ne veut pas satisfaire
- Autres types : 
    - La persona acheteur : il n'est pas utilisateur mais est celui qui va acheter
    - La persona concerné : il n'est pas utilisateur mais est concerné par l'utilisation du produit (un patient dont les données sont dans un logiciel de gestion de santé)
- Qui a le droit à la parole ?
    - Tous les personas ont le droit à la parole
    - Il faut essayer de tous les satisfaire en même temps
    - En cas de désaccord, le persona primaire l'emporte toujours
- Combien ?
    - Selon le projet
    - Suffisamment pour couvrir l'ensemble de la cible
    - Problèmes si on en crée pas assez : 
        - Passer à côté d'éléments importants
        - Ne pas satisfaire certains utilisateurs
    - Prbolèmes si on en crée trop : 
        - Trop difficile de tous les mémoriser
        - Trop difficile de les mettre d'accord
- Qui crée les personas ?
    - En équipe
    - Si on les crée seul on risque de ne pas les utiliser
- Quels éléments doit-on définir ?
    - Objectifs précis, missions sur le site
    - Relation à la marque, historique client, relation aux concurrents
    - Habitudes, envies
    - Compétences techniques
    - Equipement informatique
    - Nom, prénom, photo

#### Exercice : créer des personas pour son application

Voir : https://gitlab.inria.fr/learninglab/formation-ux/blob/master/personas.md

## Tester la cible

### Mesurer le succès

- Le taux de succès pour la réalisation d'une tâche
- Le nombre d'erreurs effectuées
- Le temps d'execution de chaque tâche
- Le nombre d'étapes nécessaires à la complétion de la tâche
- Le recours éventuel à un support

### Design thinking

**Design** = Conception
Dessin (croquis, maquette, plan)


Design thinking 101 : 

- Identifie un problème
- Réfléchit à sa solution
- Conçoit un prototype
- Le teste
- Implémentation

![Diagramme Design Thinking](https://pbs.twimg.com/media/DiZONyBXcAAZbgP?format=jpg&name=large)

#### 3 critères pour valider une idée

- Désirable et pertinente
- Faisable (techniquement)
- Viable (économiquement)

### Les tests utilisateurs

**Prérequis :**

- Avoir définit les objectifs principaux et les micro-utilités
- Avoir définit la cible

**Déroulement :**

- Définir un plan de test
- Tester le scénario soi-même
- Choisir des testeurs
- Test sous nos yeux et relever tout ce qui se passe
- Debrief et conclure
- En tirer des recommandations

**Les participants :** Au plus proche de la cible

- Site grand public (nimporte qui)
- Intranet (tous les employés de l'entreprise)
- La qualité de la sélection importante
- Si on manque de participant idéal, un participant hors cible est mieux que rien
- Doivent jouer le jeu
- Expressif (communication verbale et non verbale) surtout quand il est en difficulté
- La communication verbale est subjective et filtrée alors que non verbale en dit souvent plus (moment hésitation, soupir, agacement, etc)
- Si coincé relancer l'utilisateur sans lui donner la solution
- L'utilisateur ne doit pas faire de commentaire (sur le design ou sur son avis) mais simplement réaliser la tâche demandée

**Déterminer le nombre de participants :**
- de 5 à 20 utilisateurs
- Homogénéité de la cible
- Variabilité du plan de test (plus ils sont libres, plus ils prendront des chemins différents)

#### Supports de test

##### Prototype papier

- Permet de corriger rapidemment 
- L'utilisateur pointe les élements
- Chaque feuille représente une interface et l'encadrant se charge de répondre à ces actions

##### Prototype semi-fonctionnel

- Sur écran
- Plus fluide et proche de la réalité
- Meilleurs résultats si design final

##### Site final

- Test possible sur le concurrents
- Site fonctionnel en ligne ou non, si l'utilisateur l'a déjà visité ou non
- Permet de vérifier 

#### Plan de test

##### Définir le scénario

- (cf power point)
- Plus c'est réaliste plus c'est facile
- Scénario ouvert (laisse beaucoup de liberté) / scénario fermé (objectif précis)

##### Compréhension de l'interface

- Demander au participant de donner sa première impression
- Questions sur la compréhension de l'interface
- Peut se faire sur le store de l'application dans le cas du mobile

##### Ordre et contrebalancement

Changer l'ordre des scénario pour les différents testeurs.

##### Erreurs à eviter

- Le vocabulaire du test utiliser ne doit pas être le même que celui de l'interface
- Ne doit pas détailler les étapes permettant de réaliser la tâches
- Ce n'est pas un questionnaire
- Ne doit pas réaliser nos propres objectifs mais celui des personas
- Un plan de test ne s'écrit pas seul

#### Construction des objectifs

- Affecter à chacun des critères des échelles d'acceptabilité
    - Nombre d'eerurs
    - Durée maximale
    - Nombre de clics maximal pour trouver une information ou une action donnée
    - Poucentage d'echec
- Ne pas définir des objectifs trop éléver si la quasi totalité des utilisateurs parviennent à la réaliser

#### Pendant le test

- 1h maximum

##### Animateurs (Chargés de test)

- Gère le test de A à Z
- S'approprie l'application
- Qui doit se charger des tests ?
    - Interne : plus crédible, connait mieux mais perd en objectivité
    - Externe : indépendant, plus impartial, propose des solutions différentes mais ne comprend pas tous les détails ou le vocabulaire
- Accompagne le participant
- Requiert de grandes qualités humaines
- Invite le testeur à parler
- Mettre à l'aise le participant
- Prend des notes

##### Observateurs

- Chargés de test
- Responsables
- ...
- Dans le silence absolu (pas de commentaire)

##### Mise en place

- Se rapprocher d'une situation réelle
- Bien préparer la salle et les accessoires
- Questionnaire préliminaire
- Initier la conversation avec la personne
- Accord pour filmer, pour ses données

##### Mise en situation

- Introduire ou non le contexte
- Décrire ou non l'interface (déconseillé)
- Démarer matériel d'enregistrement

##### Présenter le plan de test

- Oral ou écrit
- Si présenté à l'oral bien définir le etxte au mot près (pour ne pas donner d'indice)

##### Prendre des notes

- Prévoir le support de notes pré-structuré
- Sur les humeurs
- Sur les erreurs
- Sur la réussite ou non de l'objectifs
- Ne pas se concentrer sur ses notes
- **Aider = fausser le test**

##### Debrief

- Recueillir son avis
- Expliquer certaines phase
- Remunération

##### Reset

- Supprimer toutes les données
- Ranger la salle
- Effacer toute trace du passage du testeur précédent
