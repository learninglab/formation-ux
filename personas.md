# Personas ePoc

## Template :

**Prénom, nom, photo**

**Information sociodémocratiques**

**Objectif précis, missions sur le site**

**Relation à la marque, historique, relations aux concurrents**

**Habitudes, envies**

**Compétence technique**

**Equipement informatique**


## Persona 1 (Claire) :

**Prénom, nom, photo**

Claire Martinello 

<img src="https://cdn.pixabay.com/photo/2017/09/21/19/06/woman-2773007_960_720.jpg" width="200px"/>

**Information sociodémocratiques**

- Femme, 43 ans, Française, Master, Mariée, 2 enfants
- Doit suivre la formation pour obtenir l'attestation sur demande de son employeur
- Femme très active / speed avec charge mentale importante

**Objectif précis, missions sur le site**

- Se former pour obtenir une attestation

**Relation à la marque, historique, relations aux concurrents**

- N'a jamais utilisé l'application et n'a jamais suivi de cours de e-learning

**Habitudes, envies**

- Se former en présentiel ou via les canaux de formation
- Suit méticuleusement les séquences dans l'ordre de la formation
- Il souhaite pouvoir échanger sur le contenu du cours

**Compétence technique**

- Compétences en informatique et mobile standard

**Equipement informatique**

- iPhone
- PC windows

## Persona 2 (Christophe) :

**Prénom, nom, photo**

Christophe Charels

<img src="https://cdn.pixabay.com/photo/2016/02/19/10/56/man-1209494_960_720.jpg" width="200px"/>

**Information sociodémocratiques**

- Homme, 50 ans, père de famille, cadre sup
- Posé, gère bien son temps
- Technophile, curieux

**Objectif précis, missions sur le site**

- Se forme pour sa culture générale sur son temps libre sans chercher l'attestation ou répondre aux questions

**Relation à la marque, historique, relations aux concurrents**

- Aucune
- Recommendation par un media réputé (radios, journaux)

**Habitudes, envies**

- Consulte régulièrement les sites de elearning
- Exigent sur l'interface de l'application
- Envie d'une application agréable

**Compétence technique**

- Sensible et utilisateur régulier de l'informatique et du mobile

**Equipement informatique**

- iPhone
- Macintosh

## Persona 3 (Arun)

**Prénom, nom, photo**

Arun Bhuvan

<img src="https://cdn.pixabay.com/photo/2014/08/06/11/19/businessman-411487_960_720.jpg" width="200px"/>

**Information sociodémocratiques**

- Homme, 35 ans, Indien, célibataire
- Post doctorant à l'Inria 
- Très exigeant sur le contenu et cartésien
- Culture non occidentale

**Objectif précis, missions sur le site**

- Cherche de la connaissance certifiée et reconnue

**Relation à la marque, historique, relations aux concurrents**

- Aucune 
- Très au fait des plateformes de e-learning (khan academy, udemy, coursera)

**Habitudes, envies**

- Va droit au but pour chercher l'information pertinente
- Consulte des publications scientifiques
- Consulte régulièrement des ressources de notoriété verifiée pour son travail/apprentissage
- N'a pas d'heure de travail, peut se former n'importe quand
- Exigent sur l'application et son contenu

**Compétence technique**

- Informaticien accompli

**Equipement informatique**

- PC Linux
- Smartphone android

## Autres personas

- Enseignant qui va utiliser le cours en classe inversée
- Retraité curieux qui aime apprendre de nouvelle chose
- Etudiant qui doit se former pour sa formation (cherche un côté plus ludique)

