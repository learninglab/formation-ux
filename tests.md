# Scénarios de tests

## Scénario 1

- A quoi sert cette page ? (Page d'accueil)
- Cherchez le résumé du livre sur la sécurité
- Commencez à lire le livre
- Allez sur le chapitre "Suite du cours ZRR"
- Donnez tous les sujets abordez par la formation
- Marquez la page "Chaptez 2 : La suite du cours"
- Rendez-vous sur une page marqué
- Faites tous les exercices
- En supposant que vous ayez juste à toutes les question, consultez votre attestation
- Reprenez la lecture du livre
- Trouvez les CGU
- Vous souhaitez faire la formation "Robot cool" que faites vous ?
- En supposant que vous ayez terminé votre livre, libérez la place sur votre téléphone